<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:date="http://exslt.org/dates-and-times"
    extension-element-prefixes="date">

    <xsl:import href="SIS_Embed.xsl" />

    <xsl:output
        method="xml"
        doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" 
        doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" />

    <xsl:param name="includeId" select="'on'"/>
    <xsl:param name="tableClasses" select="'table table-striped table-bordered'"/>
    <xsl:param name="showHeadings" select="'on'" />
    <xsl:param name="embedUrl" />
    <xsl:param name="jwt" />

    <xsl:template match="/">
        <xsl:comment> Howdy source code diver! Append format_output=on to the URL to get output that PHP calls "formatted". At least its no longer all in one line :) </xsl:comment>
        <html lang="de">
            <head>
                <meta charset="utf-8"/>
                <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <title>SIS XML XSL Export</title>

                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>
                <style> body { width: 50%; margin: auto; margin-top: 10px; } </style>
            </head>
            <body>
                <xsl:apply-imports />

                <xsl:if test="$embedUrl">
                    <h2>&lt;script&gt;-Embed</h2>
                    <p><code> &lt;script src="<xsl:value-of select="$embedUrl" />"&gt;&lt;/script&gt; </code></p>
                    <p>Mit diesem Code kann die Tabelle oben in eine Seite eingebettet werden.</p>
                    <p>Die Überschrift über der Tabelle wird nicht mit eingefügt. (Die Spaltenüberschriften bleiben davon unberührt.)</p>
                </xsl:if>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
