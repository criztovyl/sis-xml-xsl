<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:date="http://exslt.org/dates-and-times"
    extension-element-prefixes="date">


    <xsl:param name="includeId" />
    <xsl:param name="tableClasses" />
    <xsl:param name="showHeadings" />
    <xsl:param name="jwt" />

    <xsl:template match="/">
        <div class="sistable">
            <xsl:apply-templates />
        </div>
    </xsl:template>

    <xsl:template match="AlleSpiele|LetzteSpiele|NaechsteSpiele">
        <xsl:if test="$showHeadings">
            <xsl:choose>
                <xsl:when test="name(.) = 'AlleSpiele'">
                    <h1>Alle Spiele</h1>
                </xsl:when>
                <xsl:when test="name(.) = 'LetzteSpiele'">
                    <h1>Letzte Spiele</h1>
                </xsl:when>
                <xsl:when test="name(.) = 'NaechsteSpiele'">
                    <h1>Nächste Spiele</h1>
                </xsl:when>
            </xsl:choose>
        </xsl:if>
        <xsl:element name="table">
            <xsl:attribute name="class"><xsl:value-of select="$tableClasses" /></xsl:attribute>
            <tr>
                <th class="datum">Datum</th>
                <th class="zeit">Zeit</th>
                <th class="heim">Heim</th>
                <th class="gast">Gast</th>
                <th class="tore">Tore</th>
                <th class="punkte">Punkte</th>
            </tr>
            <xsl:for-each select="Spiel">
                <tr class="">
                    <td class="datum"><xsl:value-of select="concat(substring(SpielDatum, 9, 2), '.', substring(SpielDatum, 6, 2), '.', substring(SpielDatum, 1, 4))" /></td>
                    <td class="zeit"><xsl:value-of select="concat(substring(SpielVon, 12, 2), ':', substring(SpielVon, 15, 2))" /></td>
                    <td class="heim"><xsl:value-of select="Heim" /></td>
                    <td class="gast"><xsl:value-of select="Gast" /></td>
                    <td class="tore"><xsl:value-of select="Tore1" />:<xsl:value-of select="Tore2" /></td>
                    <td class="punkte"><xsl:value-of select="Punkte1" />:<xsl:value-of select="Punkte2" /></td>
                </tr>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>

    <xsl:template match="TabelleGesamtMitAK|TabelleGesamtOhneAK|TabelleHeim|TabelleAuswaerts">
        <xsl:if test="$showHeadings">
            <xsl:choose>
                <xsl:when test="name(.) = 'TabelleGesamtMitAK'">
                    <h1>Tabelle Gesamt (mit a.K.)</h1>
                </xsl:when>
                <xsl:when test="name(.) = 'TabelleGesamtOhneAK'">
                    <h1>Tabelle Gesamt (ohne a.K.)</h1>
                </xsl:when>
                <xsl:when test="name(.) = 'TabelleHeim'">
                    <h1>Tabelle Heim</h1>
                </xsl:when>
                <xsl:when test="name(.) = 'TabelleAuswaerts'">
                    <h1>Tabelle Auswärts</h1>
                </xsl:when>
            </xsl:choose>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="count(Platzierung) &gt; 0">
                <xsl:element name="table">
                    <xsl:attribute name="class"><xsl:value-of select="$tableClasses" /></xsl:attribute>
                    <tr>
                        <th class="nr">Nr</th>
                        <th class="mannschaft">Mannschaft</th>
                        <th class="spiele">Spiele</th>
                        <th class="g">G</th>
                        <th class="u">U</th>
                        <th class="v">V</th>
                        <th class="tore">Tore</th>
                        <th class="d">D</th>
                        <th class="punkte">Punkte</th>
                    </tr>
                    <xsl:for-each select="Platzierung">
                        <tr class="">
                            <td class="nr"><xsl:value-of select="Nr" /></td>
                            <td class="mannschaft"><xsl:value-of select="Name" /></td>
                            <td class="spiele"><xsl:value-of select="Spiele" />/<xsl:value-of select="SpieleInsgesamt" /></td>
                            <td class="g"><xsl:value-of select="Gewonnen" /></td>
                            <td class="u"><xsl:value-of select="Unentschieden" /></td>
                            <td class="v"><xsl:value-of select="Verloren" /></td>
                            <td class="tore"><xsl:value-of select="TorePlus" />:<xsl:value-of select="ToreMinus" /></td>
                            <td class="d"><xsl:value-of select="D" /></td>
                            <td class="punkte"><xsl:value-of select="PunktePlus" />:<xsl:value-of select="PunkteMinus" /></td>
                        </tr>
                    </xsl:for-each>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="no-data" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="Adressen">
        <xsl:if test="$showHeadings">
            <h1><xsl:value-of select="name(.)" /></h1>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="count(Adressen) &gt; 0">
                <xsl:call-template name="adressen" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="no-data" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="Spielerliste">
        <xsl:if test="$showHeadings">
            <h1><xsl:value-of select="name(.)" /></h1>
        </xsl:if>
        <xsl:element name="table">
            <xsl:attribute name="class"><xsl:value-of select="$tableClasses" /></xsl:attribute>
            <tr>
                <th class="nr">Nr</th>
                <th class="nachname">Nachname</th>
                <th class="vorname">Vorname</th>
                <th class="nation">Nation</th>
                <th class="geschlecht">Geschlecht</th>
                <th class="vereinsname">Vereinsname</th>
            </tr>
            <xsl:for-each select="Spieler">
                <tr>
                    <td class="nr"><xsl:value-of select="Nr" /></td>
                    <td class="nachname"><xsl:value-of select="NachName" /></td>
                    <td class="vorname"><xsl:value-of select="VorName" /></td>
                    <td class="nation"><xsl:value-of select="Nation" /></td>
                    <td class="geschlecht"><xsl:value-of select="Geschlecht" /></td>
                    <td class="vereinsname"><xsl:value-of select="Vereinsname" /></td>
                </tr>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>

    <xsl:template match="Paesse|Torliste|Strafen|Ligen|Gespanne|Kader">
        Nicht implementiert. (<xsl:value-of select="count(*)" /> Element(e))
    </xsl:template>

    <xsl:template match="Vereine">
        <xsl:if test="$showHeadings">
            <h1><xsl:value-of select="name(.)" /></h1>
        </xsl:if>
        <xsl:element name="table">
            <xsl:attribute name="class"><xsl:value-of select="$tableClasses" /></xsl:attribute>
            <tr>
                <th class="name">Name</th>
                <th class="homepage">Homepage</th>
                <th class="email">EMail</th>
            </tr>
            <xsl:for-each select="Verein">
                <xsl:if test="Name != '' or Homepage != '' or Email != ''">
                    <tr>
                        <td class="name"><xsl:value-of select="Name" /></td>
                        <td class="hompage"><xsl:value-of select="Hompage" /></td>
                        <td class="email"><xsl:value-of select="Email" /></td>
                    </tr>
                </xsl:if>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>

    <xsl:template match="Vereinsdaten">
        <xsl:if test="$showHeadings">
            <h1><xsl:value-of select="name(.)" /></h1>
        </xsl:if>
        <xsl:if test="count(Stammdaten) &gt; 0">
            <h2>Stammdaten</h2>
            <xsl:apply-templates select="Stammdaten" />
        </xsl:if>

        <xsl:if test="count(Mannschaften) &gt; 0">
            <h2>Mannschaften</h2>
            <xsl:call-template name="mannschaften" />
        </xsl:if>

        <xsl:if test="count(Adressen) &gt; 0">
            <h2>Adressen</h2>
            <xsl:call-template name="adressen" />
        </xsl:if>

        <xsl:if test="count(Spielklasse) &gt; 0">
            <h2>Spielklasse</h2>
            <xsl:apply-templates select="Spielklasse" />
        </xsl:if>

    </xsl:template>

    <!-- "inner" templates -->

    <xsl:template match="Stammdaten">
        <dl>
            <dt>Vereinsnr.</dt><dd><xsl:value-of select="Nr" /></dd>
            <dt>Name</dt><dd><xsl:value-of select="Name" /></dd>
        </dl>
    </xsl:template>

    <xsl:template match="Spielklasse">
        <dl>
            <dt>Name</dt><dd><xsl:value-of select="Name" /></dd>
            <dt>Mannschaften</dt><dd><xsl:value-of select="Mannschaften" /></dd>
        </dl>
    </xsl:template>

    <xsl:template name="mannschaften">
        <ul>
            <xsl:for-each select="Mannschaften">
                <li class="liga">
                    <xsl:choose>
                        <xsl:when test="$includeId">
                            <form method="post">

                                <xsl:element name="input">
                                    <xsl:attribute name="name">auf</xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="Liga" />
                                    </xsl:attribute>
                                    <xsl:attribute name="hidden" />
                                </xsl:element>

                                <xsl:element name="input">
                                    <xsl:attribute name="name">jwt</xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="$jwt" />
                                    </xsl:attribute>
                                    <xsl:attribute name="hidden" />
                                </xsl:element>

                                <input type="submit" class="btn btn-default" value="Durchsuchen" />
                                <xsl:text> </xsl:text>
                                <span><xsl:value-of select="LigaName" /></span>

                            </form>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="LigaName" />
                        </xsl:otherwise>
                    </xsl:choose>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>

    <xsl:template name="adressen">
        <xsl:element name="table">
            <xsl:attribute name="class"><xsl:value-of select="$tableClasses" /></xsl:attribute>
            <tr>
                <th class="name">Name</th>
                <th class="vorname">Vorname</th>
                <th class="straße">Straße</th>
                <th class="ort">Ort</th>
                <th class="rufnummer">Rufnummer</th>
                <th class="email">EMail</th>
                <th class="funktion">Funktion</th>
            </tr>
            <xsl:for-each select="Adresse|Adressen">
                <tr>
                    <xsl:choose>
                        <xsl:when test="FunktionsName = 'Postadresse'">
                            <td class="name"><xsl:value-of select="Name" /></td>
                            <td class="vorname"><xsl:value-of select="Vorname" /></td>
                            <td class="straße"><xsl:value-of select="Strasse" /></td>
                            <td class="ort"><xsl:value-of select="PLZORT" /></td>
                            <td class="rufnummer"><xsl:value-of select="TelHandy" /></td>
                            <td class="email"><xsl:value-of select="EMAIL" /></td>
                            <td class="funktion"><xsl:value-of select="FunktionsName" /></td>
                        </xsl:when>
                        <xsl:otherwise>
                            <td class="name"><xsl:value-of select="Name" /></td>
                            <td class="vorname"><xsl:value-of select="Vorname" /></td>
                            <td class="straße"></td>
                            <td class="ort"><xsl:value-of select="PLZORT" /></td>
                            <td class="rufnummer"></td>
                            <td class="email"></td>
                            <td class="funktion"><xsl:value-of select="FunktionsName" /></td>
                        </xsl:otherwise>
                    </xsl:choose>
                </tr>
            </xsl:for-each>
        </xsl:element>
        <p><small>Aus Datenschutzgründen wird nur die Postadresse vollständig angezeigt.</small></p>
    </xsl:template>

    <xsl:template name="no-data">
        Keine Daten.
    </xsl:template>

    <!-- Error Template (Fallback) -->

    <xsl:template match="*">
        <xsl:choose>
            <xsl:when test="name(.) = 'html'">
                <p>Ungültige Anfrage!</p>
                <p>Diese Parameter-Kombination wird von SIS nicht unterstützt.</p>
            </xsl:when>
            <xsl:otherwise>
                Incompatible with <code><xsl:value-of select="name(.)" /></code>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
