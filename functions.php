<?php

include_once("sis_schema.php");
include_once("config.php");
include_once("vendor/autoload.php");

$user= $config["user"];
$pass= $config["password"];

$BASEURL="https://www.sis-handball.de/xmlexport/xml_dyn.aspx?user=${user}&pass=${pass}";

/**
 * Request a URL from cache.
 * If not in cache or older than 30 minutes, request from api and store to cache in './cache'.
 */
function cache($url){

    $cacheDir = "cache";

    $ch = curl_init($url);
    $file = $cacheDir."/";

    // Hash the url, simplest way to get a unique, valid file name.
    $url_hash = hash('sha256', $url);

    // Use first 4 hash chars as subdir names, to prevent a single big directory
    for($i=0; $i < 4; $i++)
        $file .= $url_hash[$i]."/";

    // Create the subdirs (or even the parent cache dir if not exists)
    if(!is_dir($file)) mkdir($file, 0777, true);

    // finally: append file name
    $file .= $url_hash;
    $fileXML = $file . ".xml";

    ChromePhp::log('Requesting cache file', $fileXML);

    // Check if cache is good
    // Good: is in cache and younger than 30 min. (30*60 = 1800s)
    if(file_exists($fileXML) && (time() - filemtime($fileXML)) < 1800){
        ChromePhp::log('Found.');
        return file_get_contents($fileXML);
    } else {

        ChromePhp::log('Not found, requesting.');

        // Cache is bad, load from API
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        // Store the xml
        file_put_contents($fileXML, $output);

        // Store the url it came from
        file_put_contents($file . ".url", $url);

        // So you can quickly verify the downloaded XML in browser or however else
        // (the .. b/c $file contains the cache dir, so it wouldn't work,
        //   if you call cache/0/a/... from cache/cache.html that would result in cache/cache/0/a/...
        //   so we inject a .. so it looks that way: cache/../cache/0/a/... quick'n'dirty.)
        file_put_contents("cache/cache.html", "<a href=\"../$fileXML\">$url</a><br/>", FILE_APPEND);

        return $output;
    }
}

// Make a assoc array of the XML node's first-level children.
// Keys is child name, value how often it appears.
function children_name_list($xml){
    $ary = array();
    foreach($xml->children() as $child){
        $name = $child->getName();
        if(!isset($ary[$name]))
            $ary[$name] = 1;
        else
            $ary[$name]++;
    }
    return $ary;
}


// Call API
function get($art, $auf){

    global $BASEURL;

    if(!array_key_exists($art, SIS_SCHEMA)){
        header("Content-Type: text/plain");
        die("Invalid \"art\"!");
    }

    $art = SIS_SCHEMA[$art];

    return cache($BASEURL."&art=$art&auf=$auf");
}

/**
 * Circumvent the Request.getParam() limitation of request body overlaying query params.
 * This does exactly the opposite: Query params overlay request body.
 * For original method see:
 * https://github.com/slimphp/Slim/blob/c5dcaf41d9f5706d3f29354194663da6efcdb610/Slim/Http/Request.php#L1140
 */
function get_if_set($request, $key, $default = null){

        $postParams = $request->getParsedBody();
        $getParams = $request->getQueryParams();

        $result = $default;

        if (isset($getParams[$key])) {
            $result = $getParams[$key];
        } elseif (is_array($postParams) && isset($postParams[$key])) {
            $result = $postParams[$key];
        } elseif (is_object($postParams) && property_exists($postParams, $key)) {
            $result = $postParams->$key;
        }

        return $result;
}

/**
 *
 * Generate HTML from XML with XSL.
 *
 * @param $$router Router service
 * @param $$request
 * @param $$response
 * @param $$template template file name
 * @param $$art
 * @param $$auf
 * @param $$jwt
 * @param $$script_embed behave like <script>-Embed
 */
function xml_xls_render($router, $request, $response, $template, $art, $auf, $jwt = null, $script_embed = false){

    // we need both art and auf
    // nothing to do if either one are missing (or both)
    if(!isset($auf) || !isset($art)) return $response;

    // Make API call
    $xml=get($art, $auf);

    // Load data
    try {
        $element = new SimpleXmlElement($xml);
    } catch(Exception $e){
        return $response
            ->withHeader("Content-Type: text/plain")
            ->withStatus(500)
            ->write("Could not parse XML!\n")
            ->write(print_r($e, TRUE));
    }

    ChromePhp::log('Loaded data.');

    // Load template
    $xsl = new DOMDocument;
    $xsl->load($template);

    // Configure the transformer
    $proc = new XSLTProcessor;
    $proc->importStyleSheet($xsl); // attach the xsl rules

    $proc->setParameter("", "embedUrl", $request->getUri()->getBaseUrl() . $router->relativePathFor('embed', ['auf' => $auf, 'art' => $art]));

    if(isset($jwt))
        $proc->setParameter("", "jwt", $jwt);

    ChromePhp::log('XSLT proc set up.');

    // Ident output HTML, if requested and possible
    $format_param = get_if_set($request, "format_output");
    $format_output = !$script_embed && isset($format_param) && $format_param == "on";

    // Create HTML, enable identing, output HTML.
    $res = $proc->transformToDoc($element);
    $res->formatOutput = $format_output;
    $out = $res->saveHTML();

    ChromePhp::log('Created output.', 'script_embed', $script_embed, 'format_output', $format_output);
    ChromePhp::log('Bye!');

    if($script_embed)
        return $response->withHeader("Content-Type", "application/javascript")->write("document.write('" . str_replace("\n", "", $out) . "');");
    else return $response->write($out);

}

/**
 * Remove internal params from request params
 */
function cleared_params($request){

    $params = [
        'info' => null,
        'jwt' => null,
        'invalid' => null,
        'err_msg' => null,
    ];

    return array_merge($request->getParams(), $params);

}
