<?php

include_once "vendor/autoload.php";

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LogLevel;
use Firebase\JWT\JWT;

/**
 * Customized Slim JWT authentication middleware.
 *
 */
class MyJwtAuthentication extends Slim\Middleware\JwtAuthentication {

    /**
     * Last exception.
     *
     */
    protected $exception;

    /*
     * Override.
     *
     * Copied from original class, added storing the exception.
     * Also use getters, $options is private.
     *
     * See https://github.com/tuupola/slim-jwt-auth/blob/29b756fe660b3aae7b752a4bebf49f87266206a0/src/JwtAuthentication.php#L246
     */
    public function decodeToken($token)
    {
        try {
            return JWT::decode(
                $token,
                $this->getSecret(),
                (array) $this->getAlgorithm()
            );
        } catch (\Exception $exception) {
            $this->message = $exception->getMessage();
            $this->exception = get_class($exception); // Added [cs]
            $this->log(LogLevel::WARNING, $exception->getMessage(), [$token]);
            return false;
        }
    }

    /*
     * Override
     *
     * Copied from original class, added exception.
     * Also use getters, $options is private.
     *
     * See https://github.com/tuupola/slim-jwt-auth/blob/29b756fe660b3aae7b752a4bebf49f87266206a0/src/JwtAuthentication.php#L174
     */
    public function error(RequestInterface $request, ResponseInterface $response, $arguments)
    {
        $arguments["exception"] = $this->exception; // Added [cs]

        if (is_callable($this->getError())) {
            $handler_response = $this->getError()($request, $response, $arguments);
            if (is_a($handler_response, "\Psr\Http\Message\ResponseInterface")) {
                return $handler_response;
            }
        }
        return $response;
    }

}
