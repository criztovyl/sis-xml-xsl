#!/bin/bash
# Generate Schema for SIS XML Export
# Copyright (C) 2017 Christoph criztovyl Schulz
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# This script contains HERE-docs, which require HARD TABS!

ACTION=$1
USER=$2
PW=$3

BASEURL="https://www.sis-handball.de/xmlexport/xml_dyn.aspx?user=${USER}&pass=${PW}&auf=${USER}"
#PROXY_PARAM="--proxy localhost:3128"

SCHEMA_FILE="sis_schema.schema"
SCHEMA_LINE="%s = %s" # name content
SCHEMA_ERE="%s = [[:digit:]]+" # name
SCHEMA_EXTRACT="(.*) = (.*)" # name content

SCHEMA_PHP="sis_schema.php"
PHP_LINE='\t"%s" => %d,' # name content
PHP_LINE_REV='\t%d => "%s",' # content name
PHP_ERE='[[:space:]]+"%s" => [[:digit:]]+,' # name

escape_ere(){

	# Args: pattern
	local _pattern=$1

	# Escape a ERE string
	# https://unix.stackexchange.com/a/32396
	# User "Gilles", Feb 2012
	_pattern=${_pattern//\\//\\\\}
	for c in \[ \] \( \) \. \^ \$ \? \* \+; do
		_pattern=${_pattern//"$c"/"\\$c"}
	done

	echo ${_pattern}
	# Creates pattern for this constant. Grep if file exists, otherwise act as grep hasn't matched.
	#printf "${SCHEMA_ERE}" "${_pattern}"

}

# Mapfile callback, the magic happens here.
#
# Add's "CONST_NAME = CONTENT" (SCHEMA_LINE) lines to SCHEMA_FILE
#
# 1. Get line and line number; line is the name, the number the content.
# 2. Convert CamelCase to UPPER_CASE
# 3. Check if name is already in schema file
# 3a. No : Append
# 3b. Yes: Append content to name and add that
# 4. Partey
callback(){

	# Args: index line

	local _index=$1
	local _line=$2
	local _pattern # Pattern for checking if is already in file

	# Ignore empyt lines
	[ "${_line}" == "" ] && return;

	# Split CamelCase and sepearte by a char
	# https://stackoverflow.com/a/8503127
	# User "dogbane", Dec 2011
	# Original code inserted an "-", here it's an "_".
	_line=$(sed -e 's/\([A-Z]\)/_\1/g' -e 's/^_//'  <<< "${_line}")

	# Uppercase
	_line=${_line^^}

	# Patter to determine if schema already contains constant
	_pattern=$(printf "${SCHEMA_ERE}" "$(escape_ere "${_line}")")
	echo $_pattern
	[ -f "${SCHEMA_FILE}" ] && grep -Ex "${_pattern}" "${SCHEMA_FILE}" >/dev/null || false

	if [ $? -eq 0 ]; then
		# Constant already there, append index to name and append to file.
		printf "${SCHEMA_LINE}\n" "${_line}_${_index}" "${_index}" >> ${SCHEMA_FILE}
	else
		# Constant not there, simply append to file.
		printf "${SCHEMA_LINE}\n" "${_line}" "${_index}" >> ${SCHEMA_FILE}
	fi
}

php_schema_array_cb(){

	# Args: index line

	local _index=$1
	local _line=$2

	# Extract name & content by RegEx and append to PHP schema file
	[[ "${_line}" =~ $SCHEMA_EXTRACT ]] && printf "${PHP_LINE}\n" "${BASH_REMATCH[1]}" "${BASH_REMATCH[2]}" >> ${SCHEMA_PHP}

}

php_schema_array_rev_cb(){

	# Args: index line

	local _index=$1
	local _line=$2

	[[ "${_line}" =~ $SCHEMA_EXTRACT ]] && printf "${PHP_LINE_REV}\n" "${BASH_REMATCH[2]}" "${BASH_REMATCH[1]}" >> ${SCHEMA_PHP}

}

case "${ACTION}" in
	"schema")

		# Schema header
		# Requires hard tabs!
		cat >"$SCHEMA_FILE" <<-doc
		#
		# SIS Handball XML Export Schema
		# Generated file, do not edit.
		# Generator: $0
		# Version: $(shasum ${SCHEMA_FILE} | cut -d" " -f1)
		doc

		# Iterate over tables, receive first line (XML root node).
		# If it's no table, first line is empty.
		schema_max=35
		for ((i=0; i <= schema_max; i++)); do
			echo -ne "\033[2K\r($i/$schema_max)" >&2
			echo $(curl $PROXY_PARAM -s "${BASEURL}&art=${i}" | head -n 1) # extra echo to ignore line breaks
		done |
		# Remove HTTP \r\n, extract root node name and run callback
		sed -e 's/\//' | perl -pe 's/<(.*?)( \/)?>/$1/' | mapfile -tc 1 -C callback
		;;

	"php")

		# Make PHP Schema

		# PHP Schema header
		cat >"$SCHEMA_PHP" <<-doc
		<?php
		/*
		 * SIS Handball XML Export Schema
		 * Generated file, do not edit.
		 * Generator: $0
		 */

		const API_VERSION = "$(shasum ${SCHEMA_FILE} | cut -d" " -f1)";

		const SIS_SCHEMA = array(
		doc

		# Make Name -> Art array
		grep -v "^#" "${SCHEMA_FILE}" | mapfile -tc 1 -C php_schema_array_cb

		# Close array, open reverse
		cat >>"$SCHEMA_PHP" <<-doc
		);

		const SIS_SCHEMA_REV = array(
		doc

		# Make Art -> Name array
		grep -v "^#" "${SCHEMA_FILE}" | mapfile -tc 1 -C php_schema_array_rev_cb

		# PHP schema footer
		echo ");" >>"${SCHEMA_PHP}"
		;;
esac
