<?php

include("sis_schema.php");
include("functions.php"); // also includes config.php, so $config is available here
include("vendor/autoload.php");

include("MyJwtAuthentication.php");

ChromePhp::getInstance()->setEnabled(isset($config['log']) ? $config['log'] : false);

use Lcobucci\JWT;
use Slim\Middleware\JwtAuthentication;

$app = new Slim\App([
    'settings' => [
        'displayErrorDetails' => $config['log'],
    ],
]);

$app->add(new MyJwtAuthentication([
    "attribute" => "jwt",
    "secret" => $config["jwt_secret"],
    "error" => function ($request, $response, $arguments) {

        global $container;

        $params = $request->getParams();
        $fbns = 'Firebase\\JWT\\';
        $errorMsg;

        switch($arguments["exception"]){

        case "{$fbns}ExpiredException":
            $errorMsg = "Sitzungsschlüssel abgelaufen.";
            break;

        case "{$fbns}SignatureInvalidException":
        case "{$fbns}UnexpectedValueException":
        case "{$fbns}BeforeValidException":
        case "{$fbns}BeforeValidException":
            $errorMsg = "Sitzungsschlüssel ungültig.";
            break;

        case "":
            $errorMsg = empty($arguments["message"]) ? "Unerwarteter, unbekannter Fehler :(" : $arguments["message"];

        default:
            $errorMsg = $arguments["exception"] . ": ". $arguments["message"] . ".";

        }

        $params = array_merge($params, [
            "template" => null,
            "jwt" => null,
            "status" => "error",
            "message" => $errorMsg,
        ]);

        return $response->withRedirect( $container['router']->pathFor('index', [ /* route args */ ], $params), 401);
    },
    "rules" => [
        new JwtAuthentication\RequestMethodRule([
            "path" => "/",
            "passthrough" => ["GET"]
        ])
    ]
]));

// https://github.com/tuupola/slim-jwt-auth/issues/25
$app->add(function($request, $response, $next) {

    $params = $request->getParams();

    if(isset($params['jwt'])){
        $token = $params["jwt"];
        if (false === empty($token)) {
            $request = $request->withHeader("Authorization", "Bearer {$token}");
        }
    }

    return $next($request, $response);
});

$container = $app->getContainer();

$container['mustache'] = function($container){
    return new Mustache_Engine;
};
$container['jwt_config'] = array(
    'iss' => 'de.joinout.criztovyl.sisxmlbrowser',
    'aud' => $_SERVER['REMOTE_ADDR'],
    'secret' => $config['jwt_secret'],
    'exp' => 300, // 60*5
    'signer' => new JWT\Signer\Hmac\Sha256(),
);

$container['jwt_builder'] = function($container){
    return new JWT\Builder();
};

$container['token'] = $container->factory(function($container){

    $cfg = $container['jwt_config'];

    return $container['jwt_builder']
        ->unsign()  // clear
        ->setIssuer($cfg['iss'])
        ->setAudience($cfg['aud'])
        ->setId(rand()) // rand is only pseudorandom, but its here just for generating different JWT each call (even if same time)
        ->setIssuedAt(time())
        ->setNotBefore(time())
        ->setExpiration(time() + $cfg['exp'])
        ->sign($cfg['signer'], $cfg['secret'])
        ->getToken();
});

$app->post('/', function($request, $response){

    global $config;

    $apikeys = $config['api_keys']; // for now

    $hash_embed = hash('md5', 'SIS_Embed');

    $art = get_if_set($request, "art");
    $auf = get_if_set($request, "auf");
    $jwt = $request->getParam('jwt');
    $script_embed = get_if_set($request, 'script_embed');
    $key = get_if_set($request, 'key');

    ChromePhp::log('Hello, Console!');

    $isEmbed = false; // wrong route... ( POST / isn't embeddable)
    $hasKey = isset($key);
    $validKey = in_array($key, $apikeys);

    $valid = $isEmbed && $hasKey && $validKey; // Embed w/o JWT is only allowed with API key.

    $valid = $valid || isset($jwt); // attribute only set if token valid

    if(!$valid && $isEmbed && ( !$hasKey || !$validKey)){

        ChromePhp::warn('Missing API Key!');

        header('http/1.0 403 forbidden');
        header('Content-Type: application/javascript');
        echo "<!-- // " . ( $hasKey ? "Invalid API key." : "Missing API key." ) . " -->";
        exit;
    }

    ChromePhp::log('Valid API key', $validKey, 'Valid', $valid);

    // Basic if-else: if we have enough data to da an API call: do it.
    // If not, show a form where you can enter the required information.

    if(isset($auf) && isset($art)) {

        ChromePhp::log('Enough data to start a query.');

        return xml_xls_render($this->router, $request, $response, 'xsl/SIS_Full.xsl', $art, $auf, $jwt);

    } else {

        ChromePhp::log('Missing some data, redirecting...', 'art', $art, 'auf', $auf);

        $params = array_merge(cleared_params($request), [
            "art" => $art,
            "auf" => $auf,
            "info" => !isset($art) && !isset($auf) ? "Bitte Entität und Art auswählen." : !isset($art) ? "Bitte Art auswählen." : "Bitte Entität auswählen.",
        ]);

        return $response->withRedirect( '.' . $this->router->relativePathFor('index', [ /* route args */ ], $params));
    }
})->setName('templateRender');

$app->get('/', function($request, $response){

    $art = get_if_set($request, "art");
    $auf = get_if_set($request, "auf");
    $hash_full = hash('md5', 'SIS_Full');

    ChromePhp::log('Not enough data.');

    $schemas = array();

    foreach(SIS_SCHEMA as $key => $value)
        array_push($schemas, array('key' => $key, 'selected' => isset($art) && $key == $art));

    ChromePhp::log('Rendering form... bye!');

    $response->write($this->mustache->render(file_get_contents("index.mustache"), array(
        'schema' => $schemas,
        'auf' => $auf,
        'art' => $art,
        'jwt' => $this->token,
        'year' => date('Y'),
        'info' => $request->getParam('info'),
        'invalid' => $request->getParam('status') == "error",
        'err_msg' => $request->getParam('message'),
        'action' => '.' . $this->router->relativePathFor('templateRender', [ /* route args */ ], cleared_params($request)),
    )));

})->setName('index');

$app->get('/embed/{auf}/{art}', function($request, $response, $params){

    $auf = $params["auf"];
    $art = $params["art"];

    return xml_xls_render($this->router, $request, $response, 'xsl/SIS_Embed.xsl', $art, $auf, null, true);

})->setName('embed');

$app->run();
