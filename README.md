# SIS XML Export browser and table generator
Lets you browse the SIS-Handball XML-API and generate tables from the responses.

## index.php
Renders HTML from the XML response, using XSL(T).

It's a user-friendly interface where you can choose what kind of data you want to process and which template you want to use.
You need to specify the record ID ("auf"), but not all records are compatible with all API calls (identified by "art").

## xml\_browse.php
Shows the resulting XML the API sent, and some basic statistics about the response.

It tells you which first-level children the root has and also how often each child appears. You also can specify an XPath and you will get a `print_r` for that node list.

## xml\_xsl.php.
Lets you design XSL stylesheets.

It will show you the same statistics as xml\_browse.php. You can also enter a XSL stylesheet you want the data to be rendered with, the result can be show as rendered or raw HTML.

## make\_schema.bash
Generates the current SIS API "Schema", which is `sis_schema.schema` which contains a list of valid API calls ("art"s) and their corresponding response item.
In example `&auf=1` responds with `<AlleSpiele>` (all games), which is written as `ALLE_SPIELE = 1` in `sis_schema.schema`.

# Configuration
You will need to add your SIS credentials to an `config.php`, use `config.example.php` as an template.

# Author & License
Christoph "criztovyl" Schulz. Published under AGPLv3 and later.
