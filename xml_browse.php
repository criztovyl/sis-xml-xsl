<?php

include("sis_schema.php");
include("functions.php");

$art = "";
$auf = "";

$xsl = isset($_GET["xsl"]) ? $_GET["xsl"] : "";
$xpath = isset($_GET["xpath"]) ? $_GET["xpath"] : "";

$xml_str = "";

if(isset($_GET["auf"]) && isset($_GET["art"])){
    $art = $_GET["art"];
    $auf = $_GET["auf"];

    if($auf != "" && $art != "")
        $xml_str=get($art, $auf);
}

try {
    $element = $xml_str != "" ? new SimpleXmlElement($xml_str) : null;
} catch(Exception $e){
    header("Content-Type: text/plain");
    echo "Failed to parse XML!\n";
    print_r($e);
    exit();
}

?>
<!DOCTYPE html>
<html>
<head></head>
<body>
<h1>XML BROWSE</h1>
<form method="get">
<label>art <select name="art">
<?php foreach(SIS_SCHEMA as $key => $value): ?>
<option value="<?php echo $key; ?>"<?php if($key== $art) echo " selected"; ?>><?php echo $key; ?></option>
<?php endforeach; ?>
</select>
</label>
<label>auf <input type="text" name="auf" value="<?php echo $auf; ?>"/></label>
<label>XPath <input type="text" name="xpath" value="<?php echo $xpath; ?>"/></label></br>
<input type="submit"><input type="reset"> <a href="?">Auswahl löschen</a>
</form>
<p>An ein "auf" kommen:</p>
<ul>
<li>"art" "VEREINSDATEN", und den Nutzernamen als "auf"</li>
<li>als XPath <code>//Mannschaften</code></li>
<li>Absenden</li>
<li>Eine "Liga" raussuchen und die Nummer als "auf" nutzen :)</li>
</ul>

<div sytel="clear: both;"></div>

<div style="float: left; width: 50%">
<pre><code>
<?php

if($element != null){
    $element_children = children_name_list($element);

    echo $element->getName()."\n";
    print_r($element_children);

    echo "\n---\n\n";

    echo htmlentities($xml_str);

} ?>
</code></pre>


</div>
<div style="float: left; width: 50%">
<pre><code>
<?php
    if(isset($xpath) && $xpath != "")
        print_r($element->xpath($xpath));
?>
</code></pre>
</div>

</body></html>
