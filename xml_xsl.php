<?php

include("sis_schema.php");
include("functions.php");

$art = "";
$auf = "";

$xsl = isset($_GET["xsl"]) ? $_GET["xsl"] : "";
$xsl_text = isset($_GET["xsl_text"]) ? $_GET["xsl_text"] : "";
$xsl_use_text = isset($_GET["xsl_use_text"]) ? $_GET["xsl_use_text"] == "on" : "";
$show_source = isset($_GET["show_source"]) ? $_GET["show_source"] == "on" : "";

$xml_str = "";

if(isset($_GET["auf"]) && isset($_GET["art"])){
    $art = $_GET["art"];
    $auf = $_GET["auf"];

    if($auf != "" && $art != "")
        $xml_str=get($art, $auf);
}

try {
$element = $xml_str != "" ? new SimpleXmlElement($xml_str) : null;
} catch(Exception $e){
    header("Content-Type: text/plain");
    echo "Could not parse XML!\n";
    print_r($e->getTrace());
    exit;
}

?>
<!DOCTYPE html>
<html><head></head> <body>

<h1>XML XSL</h1>
<form method=get>
<label>art <select name=art>
<?php foreach(SIS_SCHEMA as $key => $value): ?>
<option value=<?php echo $key;  if($key== $art) echo " selected"; ?> ><?php echo "$key"; ?></option>
<?php endforeach; ?>
</select>
</label>
<label>auf <input type=text name=auf value="<?php echo $auf; ?>"/></label>
<label>Template <select name=xsl>
<?php foreach(array_filter(scandir("./xsl"), function($file) { return is_file("xsl/".$file); }) as $file): ?>
<option value="<?php $fileName = basename($file, ".xsl"); echo $fileName ?>" <?php if($fileName == $xsl) echo "selected"; ?>><?php echo $fileName; ?></option>
<?php endforeach; ?>
</select>
</label>
<label><input type="checkbox" name="show_source"<?php if($show_source) echo ' checked="checked"'; ?>/> Show Source</label>
<label><input type="checkbox" name="xsl_use_text"<?php if($xsl_use_text) echo ' checked="checked"'; ?>/> Use XSL from textarea.</label><br/>
<textarea name="xsl_text" style="width: 100%" rows=20><?php echo $xsl_text; ?></textarea><br/>
<input type=submit><input type=reset><a href="?">Auswahl löschen</a>
</form>

<div sytel="clear: both;"></div>

<div style="float: left; width: 50%">
<pre><code>
<?php

if($element != null){
    $element_children = children_name_list($element);

    echo $element->getName()."\n";
    print_r($element_children);

    echo "\n---\n\n";

    echo htmlentities($xml_str);
}
?>
</code></pre>


</div>
<div style="float: left; width: 50%">
<?php
if($element != null){

    $xsl_e = null;

    if($xsl_use_text && $xsl_text != ""){

        $xsl_e = new SimpleXmlElement($xsl_text);

    } else if ($xsl != ""){

        $xsl_e = new DOMDocument;
        $xsl_e->load('xsl/'.$xsl.'.xsl');
    }

    else {
        echo ":( $xsl ";
    }

    if($xsl != null){
        // Configure the transformer
        $proc = new XSLTProcessor;
        $proc->importStyleSheet($xsl_e); // attach the xsl rules

        $res = $proc->transformToDoc($element);
        $res->formatOutput = true;
        echo $show_source ? "<pre><code>".htmlentities($res->saveHTML())."</code></pre>" : $res->saveHTML();
    }

}
?>
</div>

</body></html>
